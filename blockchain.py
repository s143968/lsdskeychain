import random
import hashlib
import json
import time
import requests
import collections
import sys

from random import randrange
from threading import Thread


class Transaction:
    def __init__(self, key, value, timestamp):
        self.key        = key
        self.value      = value
        self.timestamp  = timestamp

    def json(self):
        return json.dumps({"key": self.key,
                           "value": self.value,
                           "timestamp": self.timestamp})


class Block:
    def __init__(self, prevHash, transactions, proof, difficulty):
        self.prevHash       = prevHash
        self.transactions   = transactions
        self.proof          = proof
        self.difficulty     = difficulty

    def hash(self):
        transactionsJson = BlockChain.getTransactionsJson(self.transactions)
        encoded = f"{self.prevHash}{transactionsJson}{self.proof}".encode()
        return hashlib.sha256(encoded).hexdigest()

    def json(self):
        return json.dumps({"prevHash": self.prevHash,
                           "proof": self.proof,
                           "difficulty": self.difficulty,
                           "transactions": [x.json() for x in self.transactions]})

    @classmethod
    def createFromJson(cls, blockJson):
        transactionsJson    = [json.loads(t) for t in blockJson['transactions']]
        transactions        = [Transaction(t['key'], t['value'], t['timestamp']) for t in transactionsJson]
        return Block(blockJson['prevHash'], transactions, blockJson['proof'], blockJson['difficulty'])


class BlockChain(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.chain          = [Block("This is the Genesis block.", [], 1986442, 6)]  # the chain
        self.neighbors      = []        # List of neighbors
        self.myIp           = "0.0.0.0" # Ip of node
        self.difficulty     = 2         # Min nb of zeros to find proof
        self.tpool          = []        # Transactions pool
        self.gateway        = False     # True => node is a gateway
        self.alive          = True      # Controls mining thread, setting it to false kills thread
        self.threadStarted  = False     # Indicates if mining thread started
        self.maxnbtx        = 100       # Max nb of transactions per block
        self.solvingConflict= False     # Indicates if a conflict is being solved
    def run(self):
        while(self.alive):
            # mine block if tpool not empty
            if(len(self.tpool) > 0):
                #mine block and broadcast it
                print(f"Mining. Difficulty {self.difficulty}. There are {len(self.tpool)} transactions in the pool")
                block = self.mineBlock()
                requests.get(f"http://{self.myIp}/addBlock?block={block.json()}&sender={self.myIp}&TTL=-1")
            
            #wait before mining again
            if(len(self.tpool) <= int(self.maxnbtx/2)):
                time.sleep(2)

    def init(self, myIp, nodeIp=None):
        print("Initialize node")
        # init
        self.myIp = myIp
        if(not myIp.startswith("127.0.0.")):
            self.gateway = True
        if(nodeIp is None):
            time.sleep(5)
            return

        # network discovery
        print("Discover network")
        self.neighbors = [nodeIp]
        self.discoverNetwork()

        # ask chain to neighbour
        r = self.tryGetRequestFromRandomNeighbour("http://|neighbour|/getChain")      
        if(r is None):
            return
        self.chain = [Block.createFromJson(json.loads(b)) for b in r.json()['chain']]

        # ask difficulty to neighbour
        r = self.tryGetRequestFromRandomNeighbour("http://|neighbour|/getNodeDifficulty")
        if(r is None):
            return
        self.difficulty = r.json()['diff']

    def discoverNetwork(self):
        #ask known nodes about their known nodes and add them
        if(len(self.neighbors) == 0):
            return
        r = self.tryGetRequestFromRandomNeighbour(f"http://|neighbour|/getNeighboursIP?myIp={self.myIp}")
        if(r is None): 
            return                   
        for ip in r.json()['ips']:
            self.addNeighbourIP(ip)
            
        #check that known nodes are alive else remove from list
        for ip in self.neighbors:            
            self.tryGetRequestOrDetectFailure(f"http://{ip}/ping", ip)

    def getNeighboursIP(self, gatewayToGateway):
        if(gatewayToGateway):            
            return [x for x in self.neighbors if(not x.startswith("127.0.0."))]
        else:
            return self.neighbors

    def removeNeighbour(self, nodeIp):
        if(nodeIp in self.neighbors):
            self.neighbors.remove(nodeIp)

    def addNeighbourIP(self, nodeIp):
        if(nodeIp not in self.neighbors and nodeIp != self.myIp):
            #add node if unknown
            self.neighbors.append(nodeIp)
            
            #send self.myIp to new node or remove if does not respond
            try:
                requests.get(f"http://{nodeIp}/getNeighboursIP?myIp={self.myIp}")
                print(f"{self.myIp} | Discovered node : {nodeIp}")
            except requests.exceptions.ConnectionError:
                print(f"Could not connect to node : {nodeIp}")
                self.removeNeighbour(nodeIp)

    def retrieveAll(self, key):
        values = []
        for block in self.chain:
            for t in block.transactions:
                if(key == t.key):
                    values.append(t.value)
        return values

    def retrieve(self, key):
        maxTime = 0
        value = None
        for block in self.chain:
            for t in block.transactions:
                if(key == t.key):
                    if(maxTime < t.timestamp):
                        maxTime = t.timestamp
                        value = t.value
        return value

    def addBlock(self, block, sender):
        # compute condition
        transactionsJson= self.getTransactionsJson(block.transactions)
        isValidProof    = self.isValidProof(block.prevHash, transactionsJson, block.proof, block.difficulty)
        isValidHash     = self.chain[-1].hash() == block.prevHash
        isBlockNew      = (len([b for b in self.chain
                                 if b.prevHash == block.prevHash and
                                 self.getTransactionsJson(b.transactions) == transactionsJson and
                                 b.proof == block.proof and
                                 b.difficulty == block.difficulty]) == 0)

        # add block or ignore block or recycle transaction and return False
        if(isValidHash and isValidProof and isBlockNew):
            self.chain.append(block)
        elif(isBlockNew and not isValidHash and sender != self.myIp):       
            for t in block.transactions:
                self.addTransaction(t.key, t.value, t.timestamp)     
            return False
        return True

    def addTransaction(self, key, value, timestamp):
        #Check if new transactions is in the chain or tPool and if so ignore it
        inTpool = (len([t for t in self.tpool if t.key == key and t.value == value and t.timestamp == timestamp]) != 0)
        if(self.isTransactionInChain(self.chain, key, value, timestamp) or inTpool):
            return
    
        # start mining thread once
        if(not self.threadStarted):
            self.threadStarted = True
            self.start()

        # first find the position where the tx will be placed
        t_pos = 0
        for i, t in enumerate(self.tpool):
            if timestamp > t.timestamp:
                t_pos = i
                break

        # place the tx at the right place
        self.tpool = self.tpool[:t_pos] + [Transaction(key, value, timestamp)] + self.tpool[t_pos:]

    def isValidProof(self, prevHash, transactionsJson, nonce, difficulty=None):
        #init
        if(difficulty is None):
            difficulty = self.difficulty
        
        #compute hash and return True if valid
        guess = f"{prevHash}{transactionsJson}{nonce}".encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:difficulty] == ("0" * difficulty)

    def isValidChain(self):
        prevHash = self.chain[0].hash()
        
        #check all block in the chain
        for block in self.chain[1:]:
            # if block prevHash correct
            if not prevHash == block.prevHash:
                return False

            # if block proof valid
            if not self.isValidProof(block.prevHash, self.getTransactionsJson(block.transactions), block.proof, block.difficulty):
                return False

            prevHash = block.hash()
        return True

    def mineBlock(self):
        # get transaction and prevHash
        transactions = self.tpool[:self.maxnbtx]  # take old transactions
        self.tpool = self.tpool[self.maxnbtx:]    # remove them from the tpool
        prevHash = self.chain[-1].hash()

        # compute proof of work
        nonce = 0
        while not self.isValidProof(prevHash, self.getTransactionsJson(transactions), nonce):
            nonce += 1
        proof = nonce

        #return new block
        return Block(prevHash, transactions, proof, self.difficulty)

    def isTransactionInChain(self, chain, key, value, timestamp):
        for block in chain:
            for t in block.transactions:
                if(t.key == key and t.value == value and t.timestamp == timestamp):
                    return True
        return False

    @classmethod
    def getTransactionsJson(cls, transactions):
        return json.dumps({"transactions": [x.json() for x in transactions]})
        
    def tryGetRequestOrDetectFailure(self, req, ip):
        #try request
        try:
            r = requests.get(req)
            return True, r
            
        #if ConnectionError assume node died
        except requests.exceptions.ConnectionError:
            self.removeNeighbour(ip)
            print(f"node {ip} died")
            return False, None

    def tryGetRequestFromRandomNeighbour(self, req):
        #try send request to random node until one works or no known nodes exist
        ok = False
        while(not ok):        
            #if no known node stop trying
            if(len(self.neighbors) == 0):
                print("All neighbours died.")
                return None
            
            #send request to random node
            neighbourIp = self.neighbors[randrange(len(self.neighbors))]
            ok, resp = self.tryGetRequestOrDetectFailure(req.replace("|neighbour|", neighbourIp), neighbourIp)
            
        return resp
            
    def solveConflict(self):
    
        #if a conflict is already being solved return
        if(not self.solvingConflict):
            self.solvingConflict = True
        else:
            return
            
        print("Solving conflict")
        
        # get chain from random neighbour
        r = self.tryGetRequestFromRandomNeighbour(f"http://|neighbour|/getChain")
        if(r is None):
            return
        if(len(r.json()['chain']) <= len(self.chain)):
            return            
        chain = [Block.createFromJson(json.loads(b)) for b in r.json()['chain']]

        # get difficulty from random neighbour
        r = self.tryGetRequestFromRandomNeighbour(f"http://|neighbour|/getNodeDifficulty")  
        if(r is None):
            return 
        self.difficulty = r.json()['diff']

        # replace chain if needed
        if(len(chain) > len(self.chain)):
            # read all transactions in self.chain
            # recycle transaction in pool that are not in new chain
            for block in self.chain:
                for t in block.transactions:
                    self.addTransaction(t.key, t.value, t.timestamp)

            # replace chain
            self.chain = chain
            print("chain replaced")
            
        self.solvingConflict = False