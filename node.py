from flask import Flask, request
from threading import Thread
import json
import logging
import random
import requests
import threading
import time

from blockchain import BlockChain, Block
from tools import getResponse, parse_arguments

#initialize flask and reduce verbose level
app = Flask(__name__)
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

#set constant
alive   = True
PORT_NB = 5000

class Broadcast(Thread):
    DEFAULT_TTL     = 2
    DEFAULT_FANOUT  = 3

    def __init__(self, req, ttl=DEFAULT_TTL, fanout=DEFAULT_TTL):
        Thread.__init__(self)
        self.fanout = fanout
        self.ttl    = ttl
        self.req    = req

    def run(self):
        #handle TTL
        if (self.ttl == -1):                
            self.ttl = Broadcast.DEFAULT_TTL
        else:                               
            self.ttl = int(self.ttl) - 1    
        if(self.ttl < 0):                   
            return

        #get random neighbors
        random.shuffle(b.neighbors)
        randNeighbours = b.neighbors[:self.fanout]
        
        #set TTL in request
        index = self.req.find("TTL=") + 4
        self.req = self.req[:index] + str(self.ttl)
        
        # broadcast request to random neighbors
        for neighbour in randNeighbours:
            b.tryGetRequestOrDetectFailure(self.req.replace("|neighbour|", neighbour), neighbour)

@app.route("/setNetworkDifficulty", methods=['GET'])
def setNetworkDifficulty():
    b.difficulty = int(request.args.get('diff'))
    ttl          = int(request.args.get('TTL'))
    
    #broadcast difficulty to others
    Broadcast(f"http://|neighbour|/setNetworkDifficulty?diff={b.difficulty}&TTL={ttl}", ttl).start()
    print("Difficulty changed")
    return getResponse(200, {})

@app.route("/ping", methods=['GET'])
def ping():
    return getResponse(200, {})

@app.route("/getNodeDifficulty", methods=['GET'])
def getNodeDifficulty():
    return getResponse(200, {"diff": b.difficulty})

@app.route("/isTransactionCompleted", methods=['GET'])
def isTransactionCompleted():
    key         = int(request.args.get('key'))
    val         = int(request.args.get('val'))    
    timestamp   = float(request.args.get('timestamp'))
    
    return getResponse(200, {"res": b.isTransactionInChain(b.chain, key, val, timestamp)})

@app.route("/put", methods=['GET'])
def put():
    key         = int(request.args.get('key'))
    val         = int(request.args.get('val'))
    timestamp   = float(request.args.get('timestamp'))
    
    #add transaction
    print(f"Handling put request : {key} {val}")
    threading.Thread(target=b.addTransaction, args=[key, val, timestamp]).start()    
    return getResponse(200, {})

@app.route("/ret", methods=['GET'])
def ret():
    key = int(request.args.get('key'))    
    return getResponse(200, {"value": b.retrieve(key)})
    
@app.route("/retAll", methods=['GET'])
def retAll():
    key = int(request.args.get('key'))    
    return getResponse(200, {"values": b.retrieveAll(key)})

@app.route("/addBlock", methods=['GET'])
def addBlock():
    block   = request.args.get('block')
    ttl     = int(request.args.get('TTL'))
    sender  = request.args.get('sender')

    #add block or solve conflict if needed
    if(not b.addBlock(Block.createFromJson(json.loads(block)),sender)):        
        threading.Thread(target=b.solveConflict).start()
    
    #broadcast block to others
    Broadcast(f"http://|neighbour|/addBlock?block={block}&sender={sender}&TTL={ttl}", ttl).start()    
    return getResponse(200, {})

@app.route("/getNeighboursIP", methods=['GET'])
def getNeighboursIP():
    myIp             = request.args.get('myIp')
    gatewayToGateway = (not myIp.startswith("127.0.0.") and not b.myIp.startswith("127.0.0."))
    
    #try add sender to list of known nodes
    b.addNeighbourIP(myIp)
    
    #return list of known nodes
    return getResponse(200, {"ips": b.getNeighboursIP(gatewayToGateway)})

@app.route("/getChain", methods=['GET'])
def getChain():
    return getResponse(200, {"chain": [x.json() for x in b.chain]})

@app.route("/kill", methods=['GET'])
def kill():
    print("KILLED")
    
    # kill mining thread
    b.alive = False
    if(b.threadStarted):
        b.join()
    
    # get and use shutdown function
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    
    return getResponse(200, {})

# ----------------------------------------------
# Main function
# ----------------------------------------------

b = BlockChain()
if __name__ == "__main__":
    # handle arguments
    args = parse_arguments()
    
    # IPV6 parsing
    if(args.connect is not None and "." not in args.connect):
        args.connect = "["+args.connect+"]"
    if(args.gateway is not None and "." not in args.gateway):
        args.gateway = "["+args.gateway+"]"

    # launch flask server
    if(args.gateway is None):
        threading.Thread(target=app.run, kwargs={'host': f"127.0.0.{args.nb}", 'port': PORT_NB}).start()
    else:
        threading.Thread(target=app.run, kwargs={'host': args.gateway, 'port': PORT_NB}).start()

    # wait for flask server to start
    responded = False
    while(not responded):
        try:
            if(args.gateway is None):
                requests.get(f"http://127.0.0.{args.nb}:{PORT_NB}/ping")
            else:
                requests.get(f"http://{args.gateway}:{PORT_NB}/ping")
            responded = True
        except requests.exceptions.ConnectionError:
            print("Waiting for server startup")
            time.sleep(1)
    time.sleep(5)
    
    #print that server started
    if(args.gateway is None):
        print(f"Server started with IP 127.0.0.{args.nb}")
    else:
        print(f"Server started with IP {args.gateway}")

    # initialize node
    if(args.gateway is None):
        if(args.connect is None):
            b.init(f"127.0.0.{args.nb}:{PORT_NB}")
        else:
            b.init(f"127.0.0.{args.nb}:{PORT_NB}", f"{args.connect}:{PORT_NB}")
    else:
        if(args.connect is None):
            b.init(f"{args.gateway}:{PORT_NB}")
        else:
            b.init(f"{args.gateway}:{PORT_NB}", f"{args.connect}:{PORT_NB}")

    # discover network a few times for good measure
    b.discoverNetwork()
    b.discoverNetwork()
    b.discoverNetwork()
    b.discoverNetwork()
    
    #keep main thread alive and discover network every 15 sec for new nodes
    while(b.alive):
        time.sleep(15)
        b.discoverNetwork()