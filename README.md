# LSDSKeyChain

## TODO
- [] Properly handle transactions when mining blocks
- [] Take care of TODOs and FIXMEs
- [] Functions to implement:
    - [] addBlock
    - [] epidemicBroadCast
- [] Create test and stress scripts
- [] Maybe refactor the code so that the blockchain is separated from the REST API?
- [] Maybe support different ports?
