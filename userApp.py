import requests
import sys
import time
import json
import threading

from blockchain import BlockChain, Block
from node import PORT_NB


class UserApp:
    def __init__(self, nodeIp):
        if("." not in nodeIp):
            nodeIp = f"[{nodeIp}]"
        self.gateway = nodeIp
        self.sizeThroughtputTest = 300

    def run(self):
        while(True):
            #print menu
            print("---------------------------")
            print("1. Put value")
            print("2. Put value and wait for block creation")
            print("3. Retrieve value")
            print("4. Retrieve all value")
            print("5. get chain")
            print("6. Set Difficulty")
            print("7. Kill node")
            print(f"8. Simple Throughput test on current node ({self.gateway})")
            print("9. Dual Throughput test on 127...1 and 127...2 + read from 127...3")
            print("10. Fault test - Dual Throughput Test + kill 127...4 and 127...5 after 3 and 6 sec")
            print("11. Quit")
            print("---------------------------")
            try:
                choice = int(input('Enter your choice:'))
            except ValueError:
                continue
            
            #ask for input if needed 
            if(choice in [1, 2, 3, 4]): key = int(input('Enter a key:'))
            if(choice in [1, 2, 6]):    value = int(input('Enter a value:'))
            if(choice in [7]):          ip = input('Enter an IP:')
            if(choice in [8, 9, 10]):   self.sizeThroughtputTest = int(input('Enter the size of the test:'))

            #call function corresponding to choice
            if(choice == 1):    self.put(key, value, False)
            if(choice == 2):    self.put(key, value, True)
            if(choice == 3):    self.retrieve(key)
            if(choice == 4):    self.retrieveAll(key)
            if(choice == 5):    self.getChain()
            if(choice == 6):    self.setNetworkDifficulty(value)
            if(choice == 7):    requests.get(f"http://{ip}:5000/kill")
            if(choice == 8):    self.throughtputTest(True)
            if(choice == 9):    self.dualThroughtputTest()
            if(choice == 10):   self.faultTest()
            if(choice == 11):   return

    def faultTest(self):
        #start throughtput test
        threading.Thread(target=self.throughtputTest, args=[False,"127.0.0.1"]).start()
        threading.Thread(target=self.throughtputTest, args=[True,"127.0.0.2"]).start()
        retTest = threading.Thread(target=self.retThroughtputTest, args=["127.0.0.3"])
        retTest.start()
        
        #kill nodes after some time
        time.sleep(3)
        requests.get(f"http://127.0.0.4:5000/kill")
        time.sleep(3)
        requests.get(f"http://127.0.0.5:5000/kill")
        
        #wait for throughtput test to end
        retTest.join()
        
    def dualThroughtputTest(self):
        #start throughtput test
        threading.Thread(target=self.throughtputTest, args=[False,"127.0.0.1"]).start()
        threading.Thread(target=self.throughtputTest, args=[True,"127.0.0.2"]).start()
        retTest = threading.Thread(target=self.retThroughtputTest, args=["127.0.0.3"])
        retTest.start()
        
        #wait for throughtput test to end
        retTest.join()
        
    def throughtputTest(self, offset, ip = None):
        offset = self.sizeThroughtputTest if offset else 0
        for i in range(self.sizeThroughtputTest):
            self.put(i, i + offset, False, ip)

    def retThroughtputTest(self,ip):
        #init
        tStart = time.time()
        toCheck1 = [x for x in range(self.sizeThroughtputTest)]
        toCheck2 = [x for x in range(self.sizeThroughtputTest)]
        
        #wait for all request to be completed
        comp = 1
        while(comp > 0):            
            #get chain
            r = requests.get(f"http://{ip}:{PORT_NB}/getChain")
            chain = [Block.createFromJson(json.loads(b)) for b in r.json()['chain']]
            
            #check content
            for b in chain:
                for t in b.transactions:
                    if(t.key == t.value and t.key in toCheck1):
                        toCheck1.remove(t.key)
                    if(t.key+self.sizeThroughtputTest == t.value and t.key in toCheck2):
                        toCheck2.remove(t.key)
                        
            #compute how many transactions are left 
            comp = (len(toCheck1)+len(toCheck2))/(2*self.sizeThroughtputTest)
                
        #compute and print time taken
        tEnd = time.time()
        print("Time taken for the test: ", tEnd-tStart)

    def put(self, key, value, block, ip = None):
        #put request
        ip = self.gateway if ip is None else ip
        timestamp = time.time()
        requests.get(f"http://{ip}:{PORT_NB}/put?key={key}&val={value}&timestamp={timestamp}")
        
        #wait for completion if needed
        if(block):
            r = requests.get(f"http://{ip}:{PORT_NB}/isTransactionCompleted?key={key}&val={value}&timestamp={timestamp}")
            while(not r.json()['res']):
                r = requests.get(f"http://{ip}:{PORT_NB}/isTransactionCompleted?key={key}&val={value}&timestamp={timestamp}")
            print("key-value pair confirmed")

    def getChain(self):
        #get chain
        r = requests.get(f"http://{self.gateway}:{PORT_NB}/getChain")
        chain = [Block.createFromJson(json.loads(b)) for b in r.json()['chain']]
        
        #print transactions
        print("Transactions :")
        for b in chain:
            for t in b.transactions:
                print(t.key," ",t.value)

    def retrieve(self, key):
        r = requests.get(f"http://{self.gateway}:{PORT_NB}/ret?key={key}")
        print(f"Result {r.json()['value']}")

    def retrieveAll(self, key):
        r = requests.get(f"http://{self.gateway}:{PORT_NB}/retAll?key={key}")
        print(r.json()['values'])

    def setNetworkDifficulty(self, diff):
        r = requests.get(f"http://{self.gateway}:{PORT_NB}/setNetworkDifficulty?diff={diff}&TTL=-1")
        print(f"Network difficulty changed to {diff}")


if __name__ == "__main__":
    if(len(sys.argv) < 2):
        print("Missing argument.")
        print("Please call programe like so : python userApp <nodeIP>")
        print("Press enter to quit...")
        input()
    else:
        u = UserApp(sys.argv[1])
        u.run()
