import subprocess
import sys

if __name__ == "__main__":
    if(len(sys.argv) < 2):
        print("Missing arguments. <Mandatory> [Optional]")
        print("Please call program like so : python createNetwork <sizeNetwork> [gatewayIp] [foreignGatewayIp]")
        print("Press enter to quit...")
        input()
    else:
        #handle foreignGateway if any
        foreignGateway = ""
        if(len(sys.argv) >= 4):
            foreignGateway = " --connect "+sys.argv[3]
            
        #handle gateway if any
        connectGateway = ""
        if(len(sys.argv) >= 3):
            connectGateway = "--connect "+sys.argv[2]
            subprocess.Popen(r'start /wait python node.py 0 '+foreignGateway+' --gateway ' + sys.argv[2], shell=True)
            
        #Create network
        subprocess.Popen(r'start /wait python node.py 1 '+connectGateway, shell=True)
        for i in range(int(sys.argv[1])):
            subprocess.Popen(r'start /wait python node.py ' + str(i+2) +
                             ' --connect 127.0.0.'+str(i+1), shell=True)
