import argparse
from flask import make_response
import json


def getResponse(code, data):
    resp = make_response(json.dumps(data))
    resp.status_code = code
    return resp

def isValidNodeNb(value):
    ivalue = int(value)
    if(ivalue < 0 or ivalue > 255):
        raise argparse.ArgumentTypeError("%s is an invalid node nb" % value)
    return ivalue

def parse_arguments():
    parser = argparse.ArgumentParser('BlockChain node')
    parser.add_argument("nb", type=isValidNodeNb, default=None, nargs='?',
                        help="Set node number (Must be unique locally, ignored for gateways)")
    parser.add_argument("--gateway", type=str, default=None, nargs='?',
                        help="Set node as gateway, expect ipv6 adress as of computer argument")
    parser.add_argument("--connect", type=str, default=None, nargs='?',
                        help="Set ip of existing node to connect to to start bootsrap procedure")
    args, _ = parser.parse_known_args()
    return args
